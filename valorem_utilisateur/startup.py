# standard
import logging
from logging.handlers import TimedRotatingFileHandler
from pathlib import Path
import os



# -- GLOBALS
logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s %(levelname)s %(message)s",
    handlers=[
        TimedRotatingFileHandler(
            filename=Path.home() / "Documents/qgis_journal.log",
            when="D",
            interval=1,
            backupCount=5,
        )
    ],
)


mon_utilisateur = os.getenv("USERNAME")

logging.info("-" * 80)
logging.info("QGIS Started")
logging.info(f"User: {mon_utilisateur}")
